import pydcd
import sys

# Simple script to print metadata for a list of dcd files

for filename in sys.argv[1:]:
  # The file object can be discarded after converting it to a dcd_file,
  # as the dcd_file duplicates the underlying file descriptor.
  file = open(filename, "r")
  dcd_file = pydcd.dcdfile(file)
  file.close()

  print("%s:" %filename)
  print("    Total number of frames in trajectory (nset): %d" %dcd_file.nset)
  print("    Number of particles in file (N): %d" %dcd_file.N)
  print("    Time step in time units (timestep): %f" %dcd_file.timestep)
  print("    Number of time steps per frame/LAMMPS dump frequency (tbsave): %d" %dcd_file.tbsave)
  print("    Iteration number of first frame (itstart): %d" %dcd_file.itstart)
  print("    Whether custom basis vectors are presesent (wcell): %d" %dcd_file.wcell)
